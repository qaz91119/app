/* Latest compiled and minified JavaScript included as External Resource */

$(document).ready(function () {
    var client = mqtt.connect('ws://140.131.7.50:8883');
    $(".filter-button").click(function () {
        var value = $(this).attr('data-filter');


        if (value == "all") {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }

        else {
            //            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
            //            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.' + value).hide('3000');
            $('.filter').filter('.' + value).show('3000');

        }
    });

    if ($(".filter-button").removeClass("active")) {
        $(this).removeClass("active");
    }
    $(this).addClass("active");

    $('#green').click(function () {
        client.publish('appiot', 'green');
    });
    $('#yellow').on('click', function (evt) {
        client.publish('appiot', 'yellow');
    });
    $('#red').on('click', function (evt) {

        client.publish('appiot', 'red');
    });

});