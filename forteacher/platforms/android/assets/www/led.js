// 手機設定狀態-----------------------------

client.on('connect', function () {
    client.subscribe('appiot');
})

client.on('message', function (topic, message) {
    if (topic == 'appiot') {
        console.log(message.toString('utf-8'));
        // 如果錯誤，則在toString後面加'utf8'
        if (message.toString() == 'green') {
            console.log('green');
            GLED.on();
            RLED.off();
            YLED.off();

            client.publish('appiot', '在辦公室中');
        }
        if (message.toString() == 'red') {
            console.log('red');
            GLED.off();
            RLED.on();
            YLED.off();

            client.publish('appiot', '忙碌中');
        }
        if (message.toString() == 'yellow') {
            console.log('yellow');
            GLED.off();
            RLED.off();
            YLED.on();
            bang.bang();
            client.publish('appiot', '馬上回來');
        }
    }
})
//